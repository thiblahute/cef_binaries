import glob
import hashlib
import os
import sys
import requests
import shutil
import tarfile
from pathlib import Path as P

build_dir, source_dir, cef_platform, cef_version = sys.argv[1:]

CEF_ESCAPED_DISTRIBUTION = f"cef_binary_{cef_version}_{cef_platform}"
CEF_DOWNLOAD_FILENAME = P(f"{CEF_ESCAPED_DISTRIBUTION}.tar.bz2")
CEF_EXTRACTED_DIR = P(source_dir) / CEF_ESCAPED_DISTRIBUTION
CEF_DOWNLOAD_PATH = P(build_dir) / CEF_DOWNLOAD_FILENAME
CEF_DOWNLOAD_SHA1_PATH  = f"{CEF_DOWNLOAD_PATH}.sha1"
CEF_BINARY_URL = f"https://cef-builds.spotifycdn.com/{CEF_DOWNLOAD_FILENAME}"

def download(url, output):
    with open(output, "wb") as f:
        response = requests.get(url, stream=True)
        total_length = response.headers.get('content-length')

        print(total_length, file=sys.stderr)
        if total_length is None: # no content length header
            f.write(response.content)
        else:
            dl = 0
            total_length = int(total_length)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                f.write(data)
                done = int(50 * dl / total_length)
                sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )
                sys.stdout.flush()

def check_sha1(sha1_file, checked_file):
    try:
        with open(sha1_file, 'r') as f:
            sha1 = f.read()

        sha1sum = hashlib.sha1()
        with open(checked_file, 'rb') as source:
            block = source.read(2**16)
            while len(block) != 0:
                sha1sum.update(block)
                block = source.read(2**16)
        return sha1sum.hexdigest() == sha1
    except FileNotFoundError:
        return False

if not check_sha1(CEF_DOWNLOAD_SHA1_PATH, CEF_DOWNLOAD_PATH):
    print(f"Downloading {CEF_BINARY_URL}", file=sys.stderr)
    download(CEF_BINARY_URL, CEF_DOWNLOAD_PATH)

    print(f"Downloading {CEF_BINARY_URL}.sha1", file=sys.stderr)
    download(f"{CEF_BINARY_URL}.sha1", CEF_DOWNLOAD_SHA1_PATH )
else:
    print(f"Tarball {CEF_DOWNLOAD_PATH} has already been properly downloaded", file=sys.stderr)

bindir = CEF_EXTRACTED_DIR.parent / "bin"
try:
    with open(bindir / 'sha1', 'r') as f:
        sha1 = f.read()

    with open(CEF_DOWNLOAD_SHA1_PATH, 'r') as f:
        wanted_sha1 = f.read()

    if sha1 != wanted_sha1:
        print("Outdated binary distribution")
        raise FileNotFoundError

    print("Binaries already exist  and is up to date", file=sys.stderr)
except FileNotFoundError:
    print("Extracting file")
    with tarfile.open(CEF_DOWNLOAD_PATH) as f:
        f.extractall(source_dir)

    try:
        shutil.rmtree(bindir)
    except FileNotFoundError:
        pass
    shutil.move(CEF_EXTRACTED_DIR, bindir)
    shutil.copyfile(CEF_DOWNLOAD_SHA1_PATH, bindir / 'sha1')

cfiles = glob.glob("bin/libcef_dll/**/*.cc") + \
    glob.glob("bin/libcef_dll/**/*.h") + \
    glob.glob("bin/libcef_dll/*.cc") + \
    glob.glob("bin/libcef_dll/*.h")

print(','.join(cfiles), end='')

