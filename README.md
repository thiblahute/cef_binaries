# [Chrome Embedded Framework](https://bitbucket.org/chromiumembedded/cef/wiki/GeneralUsage) meson definition for binary distribution

This is [`meson`](https://mesonbuild.com/) files to work with [CEF] which can
be used as any subproject using the [wrap](cef_binaries.wrap)

CEF: https://bitbucket.org/chromiumembedded/cef/wiki/GeneralUsage
